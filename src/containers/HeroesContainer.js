import Heroes from '../components/heroes/Heroes';
import { connect } from 'react-redux';

const mapStateToProps = (state) => {
    return {
        heroes: state.heroes.heroesList,
        newHero: state.heroes.newHero
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        addHero: (hero) => {
            console.log(hero);
            return dispatch({ type: 'ADD_HERO', payload: hero })},
        removeHero: (index) => dispatch({ type: 'REMOVE_HERO', payload: index }),
        updateNewHero: (hero) => dispatch({ type: 'UPDATE_HERO', payload: hero })
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Heroes);