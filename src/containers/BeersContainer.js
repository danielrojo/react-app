import { connect } from 'react-redux';
import Beers from '../components/beers/Beers';

const mapStateToProps = (state) => {
    return {
        beers: state.beers.beersList,
        range: state.beers.range
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateBeers : (beers) => dispatch({ type: 'UPDATE_BEERS', payload: beers }),     
        updateRange : (range) => dispatch({ type: 'UPDATE_RANGE', payload: range })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Beers);