import { Hero } from '../models/Hero';

export const reducer = (state =
    {
        heroes: {
            heroesList: [],
            newHero : new Hero()
        },
        beers:{
            beersList: [],
            range: [4,7]
        }
    }
    , action) => {
    switch (action.type) {
        case 'ADD_HERO':
            return {
                ...state,
                heroes: {
                    heroesList: [...state.heroes.heroesList, action.payload]
                }
            }
        case 'REMOVE_HERO':
            return {
                ...state,
                heroes: {
                    heroesList: state.heroes.heroesList.filter((hero, index) => index !== action.payload)
                }
            }
        case 'UPDATE_HERO':
            return {
                ...state,
                heroes: {
                    heroesList: state.heroes.heroesList,
                    newHero: action.payload
                }
            }
        case 'UPDATE_BEERS':
            return {
                ...state,
                beers: {
                    beersList: action.payload,
                    range: state.beers.range
                }
            }
            case 'UPDATE_RANGE':
                return {
                    ...state,
                    beers: {
                        beersList: state.beers.beersList,
                        range: action.payload
                    }
                }
        default:
            return state;
    }
}