import React, { useState, useEffect } from 'react'
import { BeerModel, getBeers } from '../../models/Beer'
import Slider from '@mui/material/Slider';


function valuetext(value) {
    return `${value}°C`;
  }

function Beers(props) {
    const [beers, setBeers] = useState(props.beers)
    const [showBeers, setShowBeers] = useState(props.beers)
    const [value, setValue] = React.useState(props.range);

    const handleChange = (event, newValue) => {
      setValue(newValue);
      props.updateRange(newValue)
      setShowBeers(beers.filter(beer => beer.abv >= newValue[0] && beer.abv <= newValue[1]).sort((a, b) => a.abv - b.abv))
    };
  
    const doRequest = async () => {
        if (props.beers.length > 0) {
            setShowBeers(beers.filter(beer => beer.abv >= props.range[0] && beer.abv <= props.range[1]).sort((a, b) => a.abv - b.abv))
            return
        }
        let myBeers = await getBeers()
        setBeers(myBeers)
        setShowBeers(myBeers.filter(beer => beer.abv >= value[0] && beer.abv <= value[1]).sort((a, b) => a.abv - b.abv))
        props.updateBeers(myBeers)
    }

    useEffect(() => {
        doRequest()
    }, [])

    const beerList = showBeers.map((beer, index) => {
        return <div className="col-xs-12 col-md-6 col-lg-4 " key={beer.id}>
            <div className="card" >
                <img className="card-img-top mx-auto mt-2" src={beer.imageUrl} alt={beer.name} style={{ width: '60px' }} />
                <div className="card-body">
                    <h4 className="card-title">{beer.name}</h4>
                    <p className="card-text">{beer.tagline}</p>
                    <p className="card-text">{beer.abv}</p>
                </div>
            </div>
        </div>
    })
    return (
        <div className="container">
      <Slider
        getAriaLabel={() => 'Temperature range'}
        value={value}
        onChange={handleChange}
        valueLabelDisplay="auto"
        getAriaValueText={valuetext}
      />
            <div className="row justify-content-center align-items-center g-2">

                {beerList}
            </div>
        </div>
    )
}

export default Beers