import React, { Fragment } from 'react'
import { Typeahead } from 'react-bootstrap-typeahead'; // ES2015


function Form() {

    const [user, setUser] = React.useState({ name: '', email: '', age: 0, country: '' });
    const [validName, setValidName] = React.useState(false);
    const [validEmail, setValidEmail] = React.useState(false);
    const [validAge, setValidAge] = React.useState(false);
    const [validCountry, setValidCountry] = React.useState(false);
    const [countries, setCountries] = React.useState(['Spain', 'UK', 'France', 'Germany', 'Italy', 'Portugal']);
    const [showArray, setShowArray] = React.useState([<Fragment></Fragment>, <Fragment></Fragment>, <Fragment></Fragment>]);

    React.useEffect(() => {
        // use a regex to validate name min 3 chars and max 20 chars
        setValidName(RegExp(/^[a-zA-Z]{3,20}$/).test(user.name));
        setValidEmail(RegExp(/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/).test(user.email));
        setValidAge(user.age > 0 && user.age < 120);
        setValidCountry(countries.includes(user.country));
    }, [user]);

    React.useEffect(() => {
        console.log(validName, validEmail, validAge, validCountry);
        let temp = [...showArray];
        temp[0] = !validName ? alertArray[0] : <Fragment></Fragment>;
        temp[1] = !validEmail ? alertArray[1] : <Fragment></Fragment>;
        temp[2] = !validAge ? alertArray[2] : <Fragment></Fragment>;
        temp[3] = !validCountry ? alertArray[3] : <Fragment></Fragment>;
        setShowArray(temp);

    }, [validName, validEmail, validAge, validCountry]);

    React.useEffect(() => {
        doRequest();
    }, []);

    const doRequest = async () => {
        const response = await fetch('https://restcountries.com/v3.1/all');
        const data = await response.json();
        let res = data.map(country => country.name.common);
        setCountries(res);
    }

    const handleChange = (event) => {
        if (event.target.type === 'number') {
            setUser({ ...user, [event.target.name]: parseInt(event.target.value) });
        } else {
            setUser({ ...user, [event.target.name]: event.target.value });
        }
    }

    const submitForm = () => {
        if (validName && validEmail && validAge) {
            console.log('Form is valid');
        } else {
            console.log('Form is not valid');
        }
    }

    const alertArray = [
        <div class="alert alert-danger" role="alert">
            <strong>Error: </strong> Name must be between 3 and 20 characters
        </div>,
        <div class="alert alert-danger" role="alert">
            <strong>Error: </strong> Email is not valid
        </div>,
        <div class="alert alert-danger" role="alert">
            <strong>Error: </strong> Age must be between 1 and 120
        </div>,
        <div class="alert alert-danger" role="alert">
        <strong>Error: </strong> Country is not valid
    </div>
    ]


    return (
        <div class="container">
            <div class="mb-3">
                <label for="" class="form-label">Name</label>
                <input type="text" value={user.name}
                    class="form-control" name="name" id="" aria-describedby="helpId" placeholder="" onChange={(event) => { handleChange(event) }} />
                {showArray[0]}

            </div>
            <div class="mb-3">
                <label for="" class="form-label">Email</label>
                <input type="email" value={user.email}
                    class="form-control" name="email" id="" aria-describedby="helpId" placeholder="" onChange={(event) => { handleChange(event) }} />
                {showArray[1]}
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Age</label>
                <input type="number" value={user.age}
                    class="form-control" name="age" id="" aria-describedby="helpId" placeholder="" onChange={(event) => { handleChange(event) }} />
                {showArray[2]}
            </div>
            <div class="mb-3">
                <label for="" class="form-label">Country</label><Typeahead
                    onChange={(selected) => {
                        setUser({ ...user, country: selected[0] });
                    }}
                    onInputChange={(selected) => {
                        setUser({ ...user, country: selected });
                    }}
                    options={countries}
                />
                {showArray[3]}
            </div>

            <button class="btn btn-primary" onClick={submitForm()} disabled={!validName || !validEmail || !validAge}>Submit</button>
        </div>
    )
}

export default Form