/*
import React, { Component } from 'react'
import './Heroes.css'
import { Hero } from '../../models/Hero'
import HeroesList from './HeroesList';
import HeroForm from './HeroForm';

class Heroes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            heroes: [new Hero('Windstorm', 'Windstorm is a hero'),
            new Hero('Bombasto', 'Bombasto is a hero'),
            new Hero('Magneta', 'Magneta is a hero'),
            new Hero('Tornado', 'Tornado is a hero')]
            
        }
    }

    addHero(hero) {
        this.setState({ heroes: [...this.state.heroes, hero]});
    }

    render() {

        return (
            <div className="container">                            
            <HeroForm onAddHero={(hero)=>{this.addHero(hero)}}/>
                <HeroesList list={this.state.heroes}/>
            </div>
        );
    }
}

export default Heroes;
*/
import React from 'react'
import './Heroes.css'
import HeroesList from './HeroesList';
import HeroForm from './HeroForm';
import { Hero } from '../../models/Hero'


function Heroes(props) {

    const [heroes, setHeroes] = React.useState(props.heroes)

    const [newHero, setNewHero] = React.useState(props.newHero)

    const removeHero = (index) => {
        console.log('rh: ' + index + 'type: ' + typeof (index));
        // cast index to number
        index = Number(index)
        props.removeHero(index)
        setHeroes(heroes.filter((hero, i) => i !== index))
    }

    const addHero = (hero) => {
        setHeroes([...heroes, hero])
        props.addHero(hero)
    }

    const updateHero = (hero) => {
        setNewHero(hero)
        props.updateNewHero(hero)
    }

    return (
        <div className="container">
            <HeroForm onAddHero={(hero) => { addHero(hero) }} heroForm={newHero} onUpdateNewHero={(hero) => { updateHero(hero) }}/>
            <HeroesList list={heroes} onRemove={(i)=>{removeHero(i)}}/>
        </div>
    )
}

export default Heroes