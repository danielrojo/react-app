/*
import React, { Component, Fragment } from 'react';
import { Hero } from '../../models/Hero';

class HeroesForm extends Component {
    constructor(props) {

        super(props);
        this.state = {
            newHero: new Hero('Thor', 'God of Thunder')
        }
    }

    handleChange(event) {
        if (event.target.name === 'name') this.setState({ newHero: new Hero(event.target.value, this.state.newHero.description) });
        else if (event.target.name === 'description') this.setState({ newHero: new Hero(this.state.newHero.name, event.target.value) });
    }

    addHero() {
        if (this.state.newHero.name === '') return;
        this.props.onAddHero(this.state.newHero);
        this.setState({ newHero: new Hero() });
    }

    render() {
        let buttonAddHero = <button className="btn btn-primary mb-5" onClick={() => { this.addHero() }}>Add Hero</button>;
        if (this.state.newHero.name === '') buttonAddHero = <button className="btn btn-secondary mb-5" disabled>Add Hero</button>;

        return (
            <Fragment>
                <div className="mb-3">
                    <label className="form-label">Name</label>
                    <input value={this.state.newHero.name} className="form-control"
                        name="name" id="" aria-describedby="helpId" placeholder="" type="text"
                        onChange={(event) => { this.handleChange(event) }} />
                </div>
                <div className="mb-3">
                    <label className="form-label">Description</label>
                    <input value={this.state.newHero.description} type="text"
                        className="form-control" name="description" id="" aria-describedby="helpId" placeholder=""
                        onChange={(event) => { this.handleChange(event) }} />
                </div>
                {buttonAddHero}
            </Fragment>
        );
    }
}

export default HeroesForm;
*/
import React, { Fragment, useState } from 'react'
import { Hero } from '../../models/Hero';

function HeroForm({onAddHero, heroForm, onUpdateNewHero}) {
    const [newHero, setNewHero] = useState(heroForm);

    const handleChange = (event) => {
        if (event.target.name === 'name') {
            setNewHero(new Hero(event.target.value, newHero.description));
            onUpdateNewHero(new Hero(event.target.value, newHero.description));
        }
        else if (event.target.name === 'description') {
            setNewHero(new Hero(newHero.name, event.target.value));
            onUpdateNewHero(new Hero(newHero.name, event.target.value));
        }
    }

    const addHero = () => {
        if (newHero.name === '') return;
        onAddHero(newHero);
        setNewHero(new Hero());
    }

    let buttonAddHero = <button className="btn btn-primary mb-5" onClick={() => { addHero() }}>Add Hero</button>;
    if (newHero.name === '') buttonAddHero = <button className="btn btn-secondary mb-5" disabled>Add Hero</button>;
    return (
        <Fragment>
            <div className="mb-3">
                <label className="form-label">Name</label>
                <input value={newHero.name} className="form-control"
                    name="name" id="" aria-describedby="helpId" placeholder="" type="text"
                    onChange={(event) => { handleChange(event) }} />
            </div>
            <div className="mb-3">
                <label className="form-label">Description</label>
                <input value={newHero.description} type="text"
                    className="form-control" name="description" id="" aria-describedby="helpId" placeholder=""
                    onChange={(event) => { handleChange(event) }} />
            </div>
            {buttonAddHero}
        </Fragment>
    )
}

export default HeroForm