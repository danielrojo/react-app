/*
import React, { Component } from 'react';

class HeroesList extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    state = {}
    render() {

        const heroList = this.props.list.map((hero, index) =>
        <li key={index} className="list-group-item">
            <h4>{hero.name}</h4>
            <p>{hero.description}</p>
        </li>);

        return (
            <ul className="list-group">
                {heroList}
            </ul>
        );
    }
}

export default HeroesList;
*/
import React from 'react'


function HeroesList({list, onRemove}) {

    const removeHero = (event) => {
        console.log('index: ' + event.target.name);
        onRemove(event.target.name)
    }
    
    const heroList = list.map((hero, index) =>
        <li key={index} className="list-group-item">
            <h4>{hero.name}</h4>
            <p>{hero.description}</p>
            <button type="button" class="btn btn-danger" name={index} onClick={(event)=>removeHero(event)}>Remove Hero</button>
        </li>);

    return (
        <ul className="list-group">
            {heroList}
        </ul>
    )
}

export default HeroesList