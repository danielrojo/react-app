import React, { Component } from 'react'
import './Keyboard.css'

export default class Keyboard extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    handleMyClick(value) {
        this.props.onButtonClick(value);
    }

    render() {
        return (
            <table>
                <tbody>
                <tr>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(9) }}>9</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(8) }}>8</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(7) }}>7</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick('+') }}>+</button></td>
                </tr>
                <tr>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(6) }}>6</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(5) }}>5</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(4) }}>4</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick('-') }}>-</button></td>
                </tr>
                <tr>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(3) }}>3</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(2) }}>2</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(1) }}>1</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick('*') }}>X</button></td>
                </tr>
                <tr>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick('AC') }}>AC</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick(0) }}>0</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick('=') }}>=</button></td>
                    <td><button className="mybutton" onClick={() => { this.handleMyClick('/') }}>/</button></td>
                </tr>
                </tbody>
            </table>
        )
    }
}
