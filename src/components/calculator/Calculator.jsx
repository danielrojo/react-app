import './Calculator.css';
import React, { Component } from 'react';
import { CalculatorModel } from '../../models/CalculatorModel';
import Display from './Display';
import Keyboard from './Keyboard';

class Calculator extends Component {

    constructor(props) {
        super(props);
        this.state = {
          display: "",
        };
        this.calculatorModel = new CalculatorModel();
    
      }
    
      handleMyClick(value) {
        // this.setState({ display: this.state.display + value });
        if (typeof value === 'number') {
          this.setState({display: this.calculatorModel.processNumber(value)});
        } else if (typeof value === 'string') {
          this.setState({display: this.calculatorModel.processString(value)});
        }
    
      }
    
      render() {
        return (
          <div className="calculator">
            <Display text={this.state.display}></Display>
    
            <Keyboard onButtonClick={(myValue)=>{this.handleMyClick(myValue)}}></Keyboard>
          </div>
        );
      }
    }
    
    export default Calculator;