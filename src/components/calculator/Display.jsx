import React, { Component } from 'react'
import './Display.css'

export default class Display extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

  render() {
    return (
        <div className="display">{this.props.text}</div>
    )
  }
}
