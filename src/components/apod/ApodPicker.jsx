import React from 'react'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";



function ApodPicker({ date, onChangeDate }) {

    const handleDate = (date) => {
        onChangeDate(date)
    }

    return (
        <DatePicker selected={date} onChange={(date) => handleDate(date)} />
    )
}

export default ApodPicker