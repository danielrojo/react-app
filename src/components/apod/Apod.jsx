/*
import React, { Component } from 'react'
import './Apod.css'

export default class apod extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apod: {}
        }
    }

    componentDidMount() {
        console.log("Apod did mount");
        let url = "https://api.nasa.gov/planetary/apod?api_key=tqz634Z1x0LiJzjbhSyUoExrZaGKLM0MG1VnROR6";
        fetch(url)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                this.setState({ apod: data });
            });
    }

    componentWillUnmount() {
        console.log("Apod will unmount");
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div
                            className="h-100 p-5 text-black bg-light border rounded-3">
                            <h2>{this.state.apod.title}</h2>
                            <img src={this.state.apod.url} alt={this.state.apod.title} />
                            <p>{this.state.apod.explanation}</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
*/

import React, { useState, useEffect } from 'react'

import ShowApod from './ShowApod';
import ApodPicker from './ApodPicker';
import { getApod, ApodModel } from '../../models/Apod'

function Apod(props) {
    const [apod, setApod] = useState(new ApodModel());
    const [startDate, setStartDate] = useState(new Date());

    const doRequest = async () => {
        let data = await getApod(startDate);
        setApod(new ApodModel(data));
    }

    useEffect(() => { doRequest() }, [startDate])

    return (
        <div className="container">
            <ApodPicker date={startDate} onChangeDate={(date) => { setStartDate(date) }}></ApodPicker>
            <div className="row">
                <div className="col-md-12">
                    <ShowApod apod={apod}></ShowApod>
                </div>
            </div>
        </div>
    )

}

export default Apod