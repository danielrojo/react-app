import React from 'react'

function ShowApod({ apod }) {
    return (
        <div className="h-100 p-5 text-black bg-light border rounded-3">
            <h2>{apod.title}</h2>
            <div class="container-fluid d-flex">
                <img className="mx-auto" src={apod.url} alt={apod.title} />
            </div>
            <p>{apod.explanation}</p>
        </div>
    )
}

export default ShowApod