import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import Heroes from './containers/HeroesContainer';
import Calculator from './components/calculator/Calculator';
import Apod from './components/apod/Apod';
import Beers from './containers/BeersContainer';
import Form from './components/form/Form';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <Router>
        <nav>
          <Link to="/calculator">Calculator</Link> |
          <Link to="/heroes">Heroes</Link> |
          <Link to="/apod">Apod</Link> |
          <Link to="/beers">Beers</Link> |
          <Link to="/form">Form</Link>
        </nav>
        <Routes>
          <Route path="/calculator" element={<Calculator />} />
          <Route path="/heroes" element={<Heroes />} />
          <Route path="/apod" element={<Apod />} />
          <Route path="/beers" element={<Beers />} />
          <Route path="/form" element={<Form />} />
          <Route path="/" element={<Heroes />} />
          <Route path="*" element={<h1>Error</h1>} />
        </Routes>

      </Router>
    );
  }
}

export default App;