export const getBeers = async () => {
    console.log('Fetching beers...');
    const response = await fetch('https://api.punkapi.com/v2/beers')
    const data = await response.json()
    let beers = []
    beers = data.map(beer => new BeerModel(beer))
    return beers
}

export class BeerModel {

    _id = -1;
    _name = '';
    _tagline = '';
    _firstBrewed = '';
    _description = '';
    _imageUrl = '';
    _abv = -1;

    constructor(json = null) {
        if (json) {
            this.id = json.id;
            this.name = json.name;
            this.tagline = json.tagline;
            this.firstBrewed = json.first_brewed;
            this.description = json.description;
            this.imageUrl = json.image_url;
            this.abv = json.abv;
        }
    }

    get id() {
        return this._id;
    }

    set id(value) {
        this._id = value;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get tagline() {
        return this._tagline;
    }

    set tagline(value) {
        this._tagline = value;
    }

    get firstBrewed() {
        return this._firstBrewed;
    }

    set firstBrewed(value) {
        this._firstBrewed = value;
    }

    get description() {
        return this._description;
    }

    set description(value) {
        this._description = value;
    }

    get imageUrl() {
        return this._imageUrl;
    }

    set imageUrl(value) {
        this._imageUrl = value;
    }

    get abv() {
        return this._abv;
    }

    set abv(value) {
        this._abv = value;
    }

    toString() { return this.name; }
}