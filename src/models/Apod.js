import moment from 'moment/moment';

export const getApod = async (startDate) => {
    console.log('getApod');
    let url = 'https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY'
    url += `&date=${moment(startDate).format('YYYY-MM-DD')}`;
    console.log('URL: ' + url);
    const reponse = await fetch(url);
    return reponse.json();
};

export class ApodModel {

    _date = '';
    _explanation = '';
    _hdurl = '';
    _mediaType = '';
    _serviceVersion = '';
    _title = '';
    _url = '';

    constructor(json = null) {
        if (json === null) {
            return;
        }
        this.date = json.date;
        this.explanation = json.explanation;
        this.hdurl = json.hdurl;
        this.mediaType = json.media_type;
        this.serviceVersion = json.service_version;
        this.title = json.title;
        this.url = json.url;
    }

    get date() {
        return this._date;
    }

    set date(value) {
        this._date = value;
    }

    get explanation() {
        return this._explanation;
    }

    set explanation(value) {
        this._explanation = value;
    }

    get hdurl() {
        return this._hdurl;
    }

    set hdurl(value) {
        this._hdurl = value;
    }

    get mediaType() {
        return this._mediaType;
    }

    set mediaType(value) {
        this._mediaType = value;
    }

    get serviceVersion() {
        return this._serviceVersion;
    }

    set serviceVersion(value) {
        this._serviceVersion = value;
    }

    get title() {
        return this._title;
    }

    set title(value) {
        this._title = value;
    }

    get url() {
        return this._url;
    }

    set url(value) {
        this._url = value;
    }

}