export class Hero {
    _name = '';
    _description = '';
    constructor(name = '', description = '') {
        this.name = name;
        this.description = description;
    }

    get name() {
        return this._name;
    }

    set name(value) {
        this._name = value;
    }

    get description() {
        return this._description;
    }

    set description(value) {
        this._description = value;
    }

    toString() {
        return `Hero: ${this._name} - ${this._description}`;
    }
}
