
const INIT = 0;
const FIRST_FIGURE = 1;
const SECOND_FIGURE = 2;
const RESULT = 3;

export class CalculatorModel {

    currentState = 0;
    firstFigure = 0;
    secondFigure = 0;
    operator = '';
    result = 0;
    display = '';

    constructor() {}

    processNumber(value) {
        switch (this.currentState) {
          case INIT:
            this.firstFigure = value;
            this.currentState = FIRST_FIGURE;
            this.display= value.toString();
            break;
          case FIRST_FIGURE:
            this.firstFigure = this.firstFigure * 10 + value;
            this.display= this.display + value.toString();
            break;
          case SECOND_FIGURE:
            this.secondFigure = this.secondFigure * 10 + value;
            this.display= this.display + value.toString();
            break;
          case RESULT:
            this.firstFigure = value;
            this.secondFigure = 0;
            this.result = 0;
            this.operator = '';
            this.currentState = FIRST_FIGURE;
            this.display= value.toString();
            break;
          default:
            break;
        }
        return this.display;
      }
    
      isOperator(value) {
        return value === '+' || value === '-' || value === '*' || value === '/';
      }
    
      resolve() {
        switch (this.operator) {
          case '+':
            return this.firstFigure + this.secondFigure;
          case '-':
            return this.firstFigure - this.secondFigure;
          case '*':
            return this.firstFigure * this.secondFigure;
          case '/':
            return this.firstFigure / this.secondFigure;
          default:
            return 0;
        }
      }
    
      processString(value) {
        if (value === 'AC') {
          this.firstFigure = 0;
          this.secondFigure = 0;
          this.result = 0;
          this.operator = '';
          this.currentState = INIT;
          this.display= "";
          return;
        }
        switch (this.currentState) {
          case INIT:
            break;
          case FIRST_FIGURE:
            if (this.isOperator(value)) {
              this.operator = value;
              this.currentState = SECOND_FIGURE;
              this.display= this.display + value.toString();
            }
            break;
          case SECOND_FIGURE:
            if (value === '=') {
              this.result = this.resolve();
              this.display= this.display + value + this.result.toString();
              this.currentState = RESULT;
            }
            break;
          case RESULT:
            if (this.isOperator(value)) {
              this.firstFigure = this.result;
              this.secondFigure = 0;
              this.result = 0;
              this.operator = value;
              this.currentState = SECOND_FIGURE;
              this.display= this.firstFigure.toString() + this.operator;
            }
            break;
          default:
            break;
        }
        return this.display;
      }
    
}