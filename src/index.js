import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import Calculator from './components/calculator/Calculator';
import Heroes from './components/heroes/Heroes';
import Apod from './components/apod/Apod';
import Beers from './components/beers/Beers';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { reducer } from './reducers/reducer';

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/calculator",
    element: <Calculator />,
  },
  {
    path: "/heroes",
    element: <Heroes />,
  },
  {
    path: "/apod",
    element: <Apod />,
  },
  {
    path: "/beers",
    element: <Beers />,
  }
]);

const store = createStore(reducer);

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <Provider store = {store}>
      <App />
    </Provider>
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
